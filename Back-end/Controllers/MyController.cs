using Microsoft.AspNetCore.Mvc;

namespace Back_end.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MyController : ControllerBase
    {
        [HttpGet("guid")]
        public string GetGuid()
        {
            return "Get " + Guid.NewGuid();
        }

        [HttpPut("guid")]
        public string PostGuid()
        {
            return "Post " + Guid.NewGuid();
        }
    }
}
